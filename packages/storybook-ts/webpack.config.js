const path = require('path');
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: {
    'index': './src/index.ts',
    'index.min': './src/index.ts'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    libraryTarget: 'commonjs',
    library: 'index',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  devtool: 'source-map',
  // plugins: [
  //   new UglifyJsPlugin({
  //     cache: true,
  //   })
  // ],
  module: {
    rules: [{
      test: /\.tsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: [["react-app", { flow: false, typescript: true }]]
      }
    }]
  }
}