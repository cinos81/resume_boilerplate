import * as React from "react";
import { action } from "@storybook/addon-actions";
import { Button } from "./Button";

export default {
  title: "0-atoms/Button"
};

export const TextButton = () => (
  <Button onClick={action("clicked")}>Hello Button</Button>
);
