import React, { MouseEventHandler, ReactChild } from "react";

export interface IButtonProps {
  onClick?: MouseEventHandler<HTMLButtonElement>;
  children: ReactChild | ReactChild[];
}

export const Button = (props: IButtonProps) => {
  return (
    <>
      {/*language=SCSS*/}
      <style>{`
        .atom-button {
          border: none;
          margin: 0;
          overflow: visible;
          line-height: normal;
          outline: 0;
          cursor: pointer;
        }
      `}</style>
      <button className="atom-button" onClick={props.onClick}>
        {props.children}
      </button>
    </>
  );
};
